#!/usr/bin/env python
# -*- coding: utf-8 -*-
from heapq import *
import math
import random
from string import ascii_uppercase

Vert = True
Horz = False

side1 = True
side2 = False

class Board(object):
    chars = {None : '-', Horz : 'h', Vert : 'v'}
    lastMoveChars = {Horz : 'H', Vert : 'W'}
    def __init__(self, n):
        self.size = n
        self.pos = [[None for x in xrange(n)] for y in xrange(n)]
        self.turn = Vert
        self.turns = 0
        self.lastMove = []
    def color(self, (x,y), border = False):
        if border:
            vBord = y in [-1, self.size]
            hBord = x in [-1, self.size]
            if vBord and hBord:
                return None
            elif vBord:
                return Vert
            elif hBord:
                return Horz
            elif x < 0 or y < 0:
                raise IndexError
            else:
                return self.pos[x][y]
        else:
            if x < 0 or y < 0:
                raise IndexError
            else:
                return self.pos[x][y]
    def setColor(self, (x,y), newValue): self.pos[x][y] = newValue
    def cells(self): return ((x,y) for x in xrange(self.size) for y in xrange(self.size))

    def __str__(self):
        rep = "%s's turn.\n" % ("Vert" if self.turn == Vert else "Horz")
        rep += ' ' + ' '.join(ascii_uppercase[:self.size])
        rep += '\n'
        rep += '\n'.join(
            [' '*(y-len(str(y+1))+1) + str(y+1) + ' '+
                ' '.join([self.lastMoveChars[self.pos[x][y]] if self.lastMove and (x,y) == self.lastMove[-1] else self.chars[self.pos[x][y]]
                 for x in xrange(self.size)])+' ' + str(y+1)
                for y in xrange(self.size)])
        rep += '\n'
        rep += ' '*(self.size+2) + ' '.join(ascii_uppercase[:self.size])
        return rep

    def move(self, cell):
        assert self.color(cell) == None
        self.setColor(cell, self.turn)
        self.turn = not self.turn
        self.turns += 1
        self.lastMove.append(cell)
    def unmove(self, cell):
        assert self.color(cell) != None
        self.setColor(cell, None)
        self.turn = not self.turn
        self.turns -= 1
        self.lastMove.pop()

    def moveStr(self, move):
        letter = move[0]
        num = move[1:]
        x = ascii_uppercase.index(letter)
        y = range(1,self.size+1).index(int(num))
        self.move((x,y))
    
    def legalMoves(self): return (cell for cell in self.cells() if self.color(cell) == None)
    def nbors(self, (x, y)):
        return [(x+dx, y+dy) for (dx, dy) in [(-1,0), (-1,1), (0,-1), (0,1), (1,-1), (1,0)]
            if (x+dx) not in [-1,self.size] and (y+dy) not in [-1, self.size]]
    def side(self, side, player):
        xs = xrange(self.size) if player == Vert else [0] if side == side1 else [self.size-1]
        ys = xrange(self.size) if player == Horz else [0] if side == side1 else [self.size-1]
        return [(x,y) for x in xs for y in ys]

    def won(self, player):
        s1 = self.side(side1, player)
        s2 = self.side(side2, player)
        
        visited = [[False for x in xrange(self.size)] for y in xrange(self.size)]
        toVisit = s1
        
        while toVisit:
            cell = toVisit.pop()
            if self.color(cell) != player: continue
            if visited[cell[0]][cell[1]]: continue
            if cell in s2: return True
            toVisit.extend(self.nbors(cell))
            visited[cell[0]][cell[1]] = True
        
        return False
    def winner(self):
        if self.won(Vert): return Vert
        elif self.won(Horz): return Horz
        else: return None

cyclicDirs = [(-1,0), (-1,1), (0,1), (1,0), (1, -1), (0, -1)]

def bridgeCon(board):
    conPlayer = board.turn
    if not board.lastMove: return None
    offset = random.randrange(6)
    (xb, yb) = board.lastMove[-1]
    for i in xrange(6):
        (dx1, dy1) = cyclicDirs[(i+offset)%6]
        (dxc, dyc) = cyclicDirs[(i+1+offset) % 6]
        (dx2, dy2) = cyclicDirs[(i+2+offset) % 6]
        
        cell1 = (xb + dx1, yb + dy1)
        cellc = (xb + dxc, yb + dyc)
        cell2 = (xb + dx2, yb + dy2)

        if board.color(cell1, True) == conPlayer and board.color(cellc, True) == None and board.color(cell2, True) == conPlayer:
                return cellc
    
    return None

vertBlock = [((1,0), (1,-1)), ((-1,0), (-1,1))]
horzBlock = [((0,1), (-1,1)), ((0,-1), (1,-1))]

def block(board):
    blockPlayer = board.turn
    if not board.lastMove: return None
    offset = random.randrange(1)
    (xp, yp) = board.lastMove[-1]
    if blockPlayer == Vert:
        blockers = vertBlock
    else:
        blockers = horzBlock
    for i in xrange(2):
        blocker = blockers[(i+offset)%2]
        for i in xrange(2):
            block1 = blocker[i]
            block2 = blocker[(i+1)%2]
            try:
                if board.color((xp + block1[0], yp + block1[1])) != blockPlayer: continue
            except IndexError: continue
            try:
                if board.color((xp + block2[0], yp + block2[1])) != None: continue
            except IndexError: continue
            return (xp + block2[0], yp + block2[1])
    return None

class MCTS(object):
    def __init__(self, parent):
        self.parent = parent
        self.unexplored = None
        self.explored = []
        
        self.wins = 0
        self.plays = 0
    
    def interval(self):
        explore = 0 #c * math.sqrt(math.log(self.parent.plays) / self.plays)
        winrate = 1.0 * self.wins / self.plays
        return winrate + explore

    def playOut(self, board):
        if board.turns == board.size**2:
            if board.won(Vert): winner = Vert
            else: winner = Horz            
            winners = [[None for x in xrange(board.size)] for y in xrange(board.size)]
            for cell in board.cells():
                cellColor = board.color(cell)
                winners[cell[0]][cell[1]] = (cellColor == winner, cellColor)

        elif self.unexplored == None or self.unexplored:
            if self.unexplored == None:
                self.unexplored = list(board.legalMoves())
                random.shuffle(self.unexplored)
            
            cell = self.unexplored.pop()
            child = MCTS(self)
            board.move(cell)
            (winner, winners) = child.simulate(board)
            board.unmove(cell)
            
            self.explored.append((cell, child))
            self.amas(winners, board.turn)
        else:
            (cell, child) = max(self.explored, key=lambda (_,c): c.interval())
            board.move(cell)
            (winner, winners) = child.playOut(board)
            board.unmove(cell)
            self.amas(winners, board.turn)
        
        if winner == (not board.turn): self.wins += 1
        self.plays += 1
        
        return (winner, winners)
    
    def simulate(self, board):
        moves = list(board.legalMoves())
        unmoves = []
        random.shuffle(moves)
        while moves:
            bridge = bridgeCon(board)
            if bridge:
                cell = bridge
            else:
                blck = block(board)
                if blck:
                    cell = blck
                else:
                    cell = moves.pop()
            
            if board.color(cell) != None: continue
            board.move(cell)
            unmoves.append(cell)
        winner = board.winner()
        winners = [[None for x in xrange(board.size)] for y in xrange(board.size)]
        for cell in board.cells():
            cellColor = board.color(cell)
            winners[cell[0]][cell[1]] = (cellColor == winner, cellColor)
        for cell in reversed(unmoves):
            board.unmove(cell)
        
        if winner == (not board.turn): self.wins += 1
        self.plays += 1
        return (winner, winners)
    
    def amas(self, winners, childColor):
        ratio = 0.1
        enemyRatio = 0.05
        for (cell, child) in self.explored:
            (win, color) = winners[cell[0]][cell[1]]
            if color == childColor:
                if win:
                    child.wins += ratio
                child.plays += ratio
            else:
                if win:
                    child.wins += enemyRatio
                child.plays += enemyRatio
        for cell in self.unexplored:
            (win, color) = winners[cell[0]][cell[1]]
            child = MCTS(self)
            self.explored.append((cell, child))
            if color == childColor:
                if win:
                    child.wins += ratio
                child.plays += ratio
            else:
                if win:
                    child.wins += enemyRatio
                child.plays += enemyRatio

def bToMC(board):
    return MCTS(None)

def aiMove(board):
    mc = bToMC(board)
    for i in xrange(600):
        mc.playOut(board)
    
    (cell, child) = max(mc.explored, key=lambda (_,c): c.wins)
    print child.plays
    print 100.0*child.wins/child.plays
    return cell

def play(n):
    b = Board(n)
    while True:
        move = aiMove(b)
        b.move(move)
        print b
        if b.winner() != None:
            print "Vert wins" if b.winner() == Vert else "Horz wins!"
            break
        move = raw_input("Human move: ")
        b.moveStr(move)
